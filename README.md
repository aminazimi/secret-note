<h1 align="center">Welcome to Secret Note👋</h1>


### 🏠 [Homepage](https://gitlab.com/aminazimi/secret-note#readme)

## How to run project
1.) Using only the docker-compose file: 
- `docker-compose up --build`
- This will start the backe-end app on port 3000 
## Description of commands

### `docker-compose up -d --build`

Builds and starts the containers for the back-end app, MongoDB database alongside each other.
It can take a few seconds after the containers have started until the Database is fully initialized.
* Open [http://localhost:3000/](http://localhost:3000/) 

### Sample valid requests

For encryption a note
```sh
    [POST] http://localhost:3000/secrectnote/SAPMLE_NOTE
```

For getting an encrypted note
```sh
    [GET] http://localhost:3000/secrectnote/:id
```
For getting decrypted an encrypted note
```sh
    [GET] http://localhost:3000/secrectnote/:id/decrypt
```


## Install locally

```sh
git clone https://gitlab.com/aminazimi/secret-note.git
cd server
npm install
```
You can find an env sample file(sample.env) for the necessary environment variables that project needs

### Locally Usages

Run the project
```sh
npm run dev
```

Building the project
```sh
npm run build
```

Start the built project
```sh
npm run start
```

Linting the project
```sh
npm run lint
```

Prettier the source code
```sh
npm run format
```

## Author

👤 **Amin Azimi**

