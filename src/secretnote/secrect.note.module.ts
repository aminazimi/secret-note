import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { SecretNote, SecretNoteSchema } from './models/secretNote.model';
import { SecrectNoteController } from './secretNote.controller';
import { SecrectNoteService } from './secretNote.service';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forFeature([
      { name: SecretNote.name, schema: SecretNoteSchema },
    ]),
  ],
  providers: [SecrectNoteService],
  controllers: [SecrectNoteController],
})
export class SecrectNoteModule {}
