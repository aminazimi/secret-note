import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SecrectNotePostDto } from './dto/secretNote.post.dto';
import { SecrectNoteGetDto } from './dto/secretNote.get.dto';
import { Model } from 'mongoose';
import { SecretNoteDocument, SecretNote } from './models/secretNote.model';
import { InjectModel } from '@nestjs/mongoose';
import SecrectNoteEncryoptResponseDto from './dto/secretNote.encrypt.response.dto';
import SecrectNoteGetResponseDto from './dto/secretNote.get.response.model';
import NotFoundExecption from 'src/exceptions/not-found.exception';

@Injectable()
export class SecrectNoteService {
  private algorithm = 'aes-256-cbc';
  constructor(
    @InjectModel(SecretNote.name)
    private secretNoteModel: Model<SecretNoteDocument>,
    private readonly configService: ConfigService,
  ) {}

  async encrypt(
    noteDto: SecrectNotePostDto,
  ): Promise<SecrectNoteEncryoptResponseDto> {
    //loading dynamically from crypto module
    const { randomBytes, createCipheriv } = await import('crypto');
    //creating the parivate key
    const iv = randomBytes(16);
    //encrypting the node
    const cipher = createCipheriv(
      this.algorithm,
      this.configService.getOrThrow('PRIVATE_KEY'),
      iv,
    );
    let encryptedNote = cipher.update(noteDto.note, 'utf-8', 'hex');
    encryptedNote += cipher.final('hex');
    const newSecretNote = new this.secretNoteModel({
      encryptedNote,
      iv: iv.toString('base64'),
    });
    //saving encrypted note in DB
    const savedSecretNote = await newSecretNote.save();
    return {
      id: savedSecretNote._id,
    };
  }

  async getSecrectNote(
    requestedNote: SecrectNoteGetDto,
  ): Promise<SecrectNoteGetResponseDto> {
    //get encrypted note from DB based on its Id
    const secrectNote: SecretNoteDocument = await this.secretNoteModel.findById(
      requestedNote.id,
    );
    if (!secrectNote) throw new NotFoundExecption(requestedNote.id, 'Note');
    //check if saved note must be decrypted or not
    const noteMessage =
      requestedNote.mode === 'decrypted'
        ? await this.decrypt(secrectNote)
        : secrectNote.encryptedNote;

    return {
      id: secrectNote._id,
      note: noteMessage,
    };
  }

  private async decrypt(secrectNote: SecretNoteDocument): Promise<string> {
    const { createDecipheriv } = await import('crypto');
    const ivBytes = Buffer.from(secrectNote.iv, 'base64');
    const decipher = createDecipheriv(
      this.algorithm,
      this.configService.getOrThrow('PRIVATE_KEY'),
      ivBytes,
    );
    let decrypted = decipher.update(secrectNote.encryptedNote, 'hex', 'utf-8');
    decrypted += decipher.final('utf-8');
    return decrypted;
  }
}
