export class SecrectNoteGetDto {
  id: string;
  mode: 'encrypted' | 'decrypted';
}
