import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { SecrectNotePostDto } from './dto/secretNote.post.dto';
import SecrectNoteEncryoptResponseDto from './dto/secretNote.encrypt.response.dto';
import { SecrectNoteService } from './secretNote.service';
import { SecrectNoteGetDto } from './dto/secretNote.get.dto';

@Controller('secrectnote')
export class SecrectNoteController {
  constructor(private readonly secretNoteService: SecrectNoteService) {}

  @Post()
  async encrypt(
    @Body() secrectNote: SecrectNotePostDto,
  ): Promise<SecrectNoteEncryoptResponseDto> {
    return await this.secretNoteService.encrypt(secrectNote);
  }

  @Get(':id/:method')
  async getNoteByMethod(
    @Param('id') id: string,
    @Param('method') method: string,
  ) {
    const getNoteDto: SecrectNoteGetDto = {
      id,
      mode: method === 'decrypted' ? 'decrypted' : 'encrypted',
    };
    return await this.secretNoteService.getSecrectNote(getNoteDto);
  }

  @Get(':id')
  async getNote(@Param('id') id: string) {
    const getNoteDto: SecrectNoteGetDto = {
      id,
      mode: 'encrypted',
    };
    return await this.secretNoteService.getSecrectNote(getNoteDto);
  }
}
