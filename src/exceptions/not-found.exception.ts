import HttpExecption from './http.exception';

class NotFoundExecption extends HttpExecption {
  constructor(id: string, objectName: string) {
    super(`${objectName} id:${id} not found`, 404);
  }
}

export default NotFoundExecption;
